const fs = require("fs/promises");
const path = require("path");

async function problem1(data = {}, dirName = "jsonDir", numOfFiles = 1) {
    const dirPath = path.join(__dirname, dirName);

    // creating a directory
    try {
        await fs.mkdir(dirPath);
        console.log(dirPath + " directory created!");
        for (let index = 0; index < numOfFiles; index++) {
            // creating json file
            await fs.writeFile(`test${index + 1}.json`, JSON.stringify(data));
            console.log(`test${index + 1}.json File Created!`);

            // deleting json file
            await fs.unlink(`test${index + 1}.json`);
            console.log(`test${index + 1}.json file Deleted!`);
        }
    } catch (err) {
        console.error(err);
    }
}

module.exports = problem1;
