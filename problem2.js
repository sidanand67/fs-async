const fs = require('fs/promises'); 

async function problem2(filePath){
    const fileData = await fs.readFile(filePath, 'utf-8'); 

    const upperCaseFileName = await upperCaseContent(fileData); 
    const lowerCaseSplitFileName = await lowerCaseSplitContent(upperCaseFileName); 
    await sortFileContent(upperCaseFileName, lowerCaseSplitFileName);

    deleteAll(); 
}

function upperCaseContent(fileData){
    const content = fileData.toUpperCase(); 
    const fileName = "upperCaseContent.txt"; 

    writeFileContent(fileName, content); 
    writeFileNames(fileName);

    return fileName; 
}

async function lowerCaseSplitContent(file){
    const fileName = "lowerCaseSplitContent.txt"; 

    try {
        const data = await fs.readFile(file, 'utf-8'); 
        const lowerCaseSplitData = data.toLowerCase().split('.').map((line) => line.trim()).join('\n'); 

        writeFileContent(fileName, lowerCaseSplitData); 
        writeFileNames(fileName); 
    }
    catch(err){
        console.error(err); 
    }

    return fileName; 
}

async function sortFileContent(upperCaseFileContent, lowerCaseSplitContent){
    const fileName = "sortFileContent.txt"; 
    
    try{
        const upperCaseFile = await fs.readFile(upperCaseFileContent, 'utf-8'); 
        const lowerCaseFile = await fs.readFile(lowerCaseSplitContent, 'utf-8'); 
        
        const sortFileData = (upperCaseFile + lowerCaseFile).replace('\n', ' ').split(' ').map(word => word.trim()).sort().join(' '); 

        writeFileContent(fileName, sortFileData); 
        writeFileNames(fileName); 
    }
    catch(err){
        console.error(err); 
    }
}

async function deleteAll(){
    try {
        let files = await fs.readFile('fileNames.txt', 'utf-8');
        files.trim().split('\n').forEach(async (fileName) => {
            await fs.unlink(fileName); 
            console.log(fileName + ' deleted!'); 
        })
    }
    catch(err){
        console.error(err); 
    }
}

async function writeFileContent(fileName, fileData){
    try{
        await fs.writeFile(fileName, fileData); 
        console.log('Data stored in ' + fileName); 
    }
    catch(err){
        console.error(err); 
    }
}

async function writeFileNames(fileName){
    try{
        await fs.appendFile('fileNames.txt', `${fileName}\n`); 
        console.log(fileName + ' added to filenames.txt'); 
    }
    catch(err){
        console.error(err); 
    }
}

module.exports = problem2; 